package graphtest;

import java.awt.Color;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.graphstream.graph.Edge;
import org.graphstream.graph.EdgeRejectedException;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;

public class Program {
  public static void main(String[] args) {
    //System.setProperty("gs.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
    
    Graph graph = new SingleGraph("bla");
    buildGraph(graph);
    graph.display();
  }

  private static Node createOrGet(Map<String, Node> airports, Graph graph, String airportName) {
    Node resNode;
    if (airports.containsKey(airportName)) {
      resNode = airports.get(airportName);
    } else {
      resNode = graph.addNode(airportName);
      airports.put(airportName, resNode);
    }
    
    return resNode;
  }
  
  private static String colorToRgbStr(Color color) {
    return "rgb(" + color.getRed() + ", " + color.getGreen() + ", " + color.getBlue() + ")";
  }
  
  private static String getGraphStylesheet(Map<String, AirlineInfo> airlines) {
    String res = "node { size: 3; }\n";
    
    for (AirlineInfo airline : airlines.values()) {
      res += "edge." + airline.cssClass + " { fill-color: " + colorToRgbStr(airline.color) + "; }\n";
    }
    
    return res;
  }
  
  static class AirlineInfo {
    public Color color;
    public String cssClass;
  }
  
  private static AirlineInfo getAirlineInfo(Map<String, AirlineInfo> airlines, String airline) {
    if (airlines.containsKey(airline)) {
      return airlines.get(airline);
    } else {
      AirlineInfo airlineInfo = new AirlineInfo();
      Random r = new Random();
      airlineInfo.color = new Color(r.nextInt());
      airlineInfo.cssClass = "airline" + airlines.size();
      airlines.put(airline, airlineInfo);
      return airlineInfo;
    }
  }
  
  private static void buildGraph(Graph graph) {
    Connection c = null;
    try {
      Class.forName("org.sqlite.JDBC");
      c = DriverManager.getConnection("jdbc:sqlite:flights.sqlite");
      Statement stmt = c.createStatement();

      String sql = "SELECT SAirports.Name as Source, DAirports.Name as Dest, Airlines.Name as Airline FROM Routes " +
          "JOIN Airlines ON Routes.AirlineId = Airlines.Id " +
          "JOIN Airports as SAirports ON Routes.SourceAirportId = SAirports.Id " + 
          "JOIN Airports as DAirports ON Routes.DestinationAirportId = DAirports.Id";
      if (!stmt.execute(sql)) {
        throw new RuntimeException("Query failed");
      }

      Map<String, Node> airports = new HashMap<String, Node>();
      Map<String, AirlineInfo> airlines = new HashMap<String, AirlineInfo>();
      ResultSet res = stmt.getResultSet();
      for (int i = 0; i < 2000000 && !res.isAfterLast(); i++, res.next())
      {
        String sourceName = res.getString("Source");
        String destName = res.getString("Dest");
        
        Node sourceNode = createOrGet(airports, graph, sourceName);
        Node destNode = createOrGet(airports, graph, destName);
        
        try {
          String airlineName = res.getString("Airline");
          
          Edge edge = graph.addEdge("flight" + i, sourceNode, destNode);
          AirlineInfo airlineInfo = getAirlineInfo(airlines, airlineName);
          
          edge.addAttribute("ui.class", airlineInfo.cssClass);
        } catch (EdgeRejectedException e) {      
          //System.out.println(e.getMessage());
        }
      }
      
      graph.addAttribute("ui.stylesheet", getGraphStylesheet(airlines)); 

      stmt.close();
      c.close();
    } catch (ClassNotFoundException | SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
}
