package db;

import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.graphstream.graph.Edge;
import org.graphstream.graph.EdgeRejectedException;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.stream.file.FileSinkImages;
import org.graphstream.stream.file.FileSinkImages.LayoutPolicy;
import org.graphstream.stream.file.FileSinkImages.OutputType;
import org.graphstream.stream.file.FileSinkImages.Resolution;
import org.graphstream.stream.file.FileSinkImages.Resolutions;

import java.awt.Color;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FlightModel {
  public static String getFlightGraph() throws IOException, ClassNotFoundException, SQLException {
    Graph graph = new SingleGraph("bla");
    buildGraph(graph);

    OutputType type = OutputType.JPG;
    Resolution resolution = Resolutions.HD720;
    
    FileSinkImages pic = new FileSinkImages(type, resolution);
    pic.setLayoutPolicy(LayoutPolicy.COMPUTED_ONCE_AT_NEW_IMAGE);
    
    // directly writing to an output stream throws "not implemented" in the library
    // thus let it write to a file and return the filename
    UUID uuid = UUID.randomUUID();
    String filename = uuid.toString() + ".jpg";

    pic.writeAll(graph, filename);
    return filename;
  }

  private static Node getAirport(Map<String, Node> airports, Graph graph, String airportName) {
    Node resNode;
    if (airports.containsKey(airportName)) {
      resNode = airports.get(airportName);
    } else {
      resNode = graph.addNode(airportName);
      airports.put(airportName, resNode);
    }
    
    return resNode;
  }
  
  private static String colorToRgbStr(Color color) {
    return "rgb(" + color.getRed() + ", " + color.getGreen() + ", " + color.getBlue() + ")";
  }
  
  private static String getGraphStylesheet(Map<String, AirlineInfo> airlines) {
    String res = "node { size: 3; }\n";
    
    for (AirlineInfo airline : airlines.values()) {
      res += "edge." + airline.cssClass + " { fill-color: " + colorToRgbStr(airline.color) + "; }\n";
    }
    
    return res;
  }
  
  static class AirlineInfo {
    public Color color;
    public String cssClass;
  }
  
  private static AirlineInfo getAirlineInfo(Map<String, AirlineInfo> airlines, String airline) {
    if (airlines.containsKey(airline)) {
      return airlines.get(airline);
    } else {
      AirlineInfo airlineInfo = new AirlineInfo();
      Random r = new Random();
      airlineInfo.color = new Color(r.nextInt());
      airlineInfo.cssClass = "airline" + airlines.size();
      airlines.put(airline, airlineInfo);
      return airlineInfo;
    }
  }
  
  private static void buildGraph(Graph graph) throws ClassNotFoundException, SQLException {
    Connection c = null;
    Class.forName("org.sqlite.JDBC");
    c = DriverManager.getConnection("jdbc:sqlite:flights.sqlite");
    Statement stmt = c.createStatement();

    String sql = "SELECT SAirports.Name as Source, DAirports.Name as Dest, Airlines.Name as Airline FROM Routes " +
        "JOIN Airlines ON Routes.AirlineId = Airlines.Id " +
        "JOIN Airports as SAirports ON Routes.SourceAirportId = SAirports.Id " + 
        "JOIN Airports as DAirports ON Routes.DestinationAirportId = DAirports.Id";
    if (!stmt.execute(sql)) {
      throw new RuntimeException("Query failed");
    }

    Map<String, Node> airports = new HashMap<String, Node>();
    Map<String, AirlineInfo> airlines = new HashMap<String, AirlineInfo>();
    ResultSet res = stmt.getResultSet();
    for (int i = 0; i < 2000 && !res.isAfterLast(); i++, res.next())
    {
      String sourceName = res.getString("Source");
      String destName = res.getString("Dest");
      
      Node sourceNode = getAirport(airports, graph, sourceName);
      Node destNode = getAirport(airports, graph, destName);
      
      try {
        String airlineName = res.getString("Airline");
        
        Edge edge = graph.addEdge("flight" + i, sourceNode, destNode);
        AirlineInfo airlineInfo = getAirlineInfo(airlines, airlineName);
        
        edge.addAttribute("ui.class", airlineInfo.cssClass);
      } catch (EdgeRejectedException e) {      
        //System.out.println(e.getMessage());
      }
    }
    
    graph.addAttribute("ui.stylesheet", getGraphStylesheet(airlines)); 

    stmt.close();
    c.close();
  }
}
