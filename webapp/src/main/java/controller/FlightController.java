package controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import db.FlightModel;

@Controller
public class FlightController {
  
  @RequestMapping("/")
  public String index(Model model) { 
    return "index";
  }
  
  @ResponseBody
  @RequestMapping("/image")
  public ResponseEntity<InputStreamResource> testphoto() {
    try {
      String file = FlightModel.getFlightGraph();

      return ResponseEntity.ok().body(new InputStreamResource(Files.newInputStream(Paths.get(file))));
    } catch (ClassNotFoundException | SQLException | IOException e) {
      System.out.println(e.getMessage());
      throw new RuntimeException("Couldn't create graphic");
    }
  }
}
