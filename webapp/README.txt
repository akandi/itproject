This is a web application using Spring and Gradle. For Eclipse the Gradle plugin is required. Then one can Import -> 
Gradle Project -> Browse -> Build Model -> Finish. To run Window -> Show View -> Other -> Gradle Tasks -> bootRun -> 
localhost:8080.